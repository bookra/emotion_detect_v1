# Download the helper library from https://www.twpiilio.com/docs/python/install
from twilio.rest import Client
import json
import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

account_sid = os.environ.get("ACCOUNT_SID")
auth_token = os.environ.get("AUTH_TOKEN")

def sendWhatsAppMessage(content):
#    with open('.\\__config_twilio.json') as config: 
#       data =  json.load(config)
#       account_sid, auth_token = data['account_sid'], data['auth_token']
   # Your Account Sid and Auth Token from twilio.com/console
   # DANGER! This is insecure. See http://twil.io/secure
   client = Client(account_sid, auth_token)
   message = client.messages \
                .create(
                     body=content,
                    from_='whatsapp:+14155238886',
                    to='whatsapp:+14252462060'
                    #to='whatsapp:+14252478797'
                 )
   print(message.sid)

sendWhatsAppMessage("hello test")