## Facial Recognition
run `pip install -r requirements.txt`
then `python stream.py`

### to run the server:

`python api.py`
and then make a call to the api as shown in the provided postman collection


### to run docker container:
```
docker build -t deepface .
```
followed by 
```
docker run -p 5000:5000 -d deepface
```