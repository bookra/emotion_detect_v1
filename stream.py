# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
from deepface import DeepFace

# result = DeepFace.verify("./db/omar/omar1.jpg", "./db/trump/t1.jpeg","Facenet")
result = DeepFace.analyze("./db/omar/oSmall.jpeg", actions = ['age', 'gender', 'race', 'emotion'])

print(result)


# DeepFace.stream('db')


