FROM python:3.6.10-buster
WORKDIR /project
RUN apt-get update
RUN apt-get install -y libgl1
RUN pip install -r requirements.txt
dADD . /project
CMD ["python", "api.py"]